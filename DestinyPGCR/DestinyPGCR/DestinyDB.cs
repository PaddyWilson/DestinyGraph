﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;

namespace DestinyPGCR
{
	public class DestinyDB
	{
		string fileName;
		SQLiteConnection connection;

		public DestinyDB(string file)
		{
			fileName = file;
		}

		public void Connect()
		{
			Console.WriteLine(fileName);
			connection = new SQLiteConnection("Data Source=" + fileName + ";Version=3;");
			connection.Open();
		}

		public int Execute(string query)
		{
			SQLiteCommand command = new SQLiteCommand(query, connection);
			return command.ExecuteNonQuery();
		}

		public int Execute(string query, SQLiteCommand command)
		{
			command = new SQLiteCommand(query, connection);
			return command.ExecuteNonQuery();
		}

		public void Disconnect()
		{
			connection.Close();
		}

		public long GetHighest()
		{
			string sql = "SELECT MAX(game_id) AS highest FROM game;";
			SQLiteCommand command = new SQLiteCommand(sql, connection);
			SQLiteDataReader reader = command.ExecuteReader();
			while (reader.Read())
			{
				try
				{
					long i = (long)reader["highest"];
					return i;
				}
				catch (Exception)
				{
					return 0;
				}

			}
			return 0;
		}

		public List<long> FindMissing()
		{
			List<long> missing = new List<long>();

			string sql = "SELECT DISTINCT game_id +1 AS missing FROM game WHERE game_id +1 NOT IN(SELECT DISTINCT game_id FROM game); ";
			SQLiteCommand command = new SQLiteCommand(sql, connection);
			SQLiteDataReader reader = command.ExecuteReader();
			while (reader.Read())
			{
				try
				{
					long i = (long)reader["missing"];
					missing.Add(i);
				}
				catch (Exception)
				{ }
			}

			if (missing.Count > 0)
			{
				missing.RemoveAt(missing.Count - 1);
			}

			List<long> output = new List<long>();

			foreach (var item in missing)
			{
				List<long> outp = FindMissingRange(item, item+1000);
				foreach (var num in outp)
				{
					if (!output.Contains(num))
					{
						output.Add(num);
					}
				}
			}

			return output;
		}

		public List<long> FindMissingRange(long low, long high)
		{
			List<long> have = new List<long>();
			List<long> missing = new List<long>();

			string sql = "SELECT game_id FROM game WHERE game_id BETWEEN " + low + " AND " + high + "; ";
			SQLiteCommand command = new SQLiteCommand(sql, connection);
			SQLiteDataReader reader = command.ExecuteReader();
			while (reader.Read())
			{
				try
				{
					long i = (long)reader["game_id"];
					have.Add(i);
				}
				catch (Exception)
				{ }
			}

			long highest = GetHighest();

			for (long i = low; i < high; i++)
			{
				if (!have.Contains(i) && i < highest)
				{
					missing.Add(i);
				}
			}
			return missing;
		}

		public long InsertGame(long gameid, long mode, string date, string time, long ref_id, long activity_hash, long duration)
		{
			string sql = string.Format("INSERT INTO game(game_id, mode, date, time, referance_id, activity_hash, duration) " +
				"VALUES (@game_id, @mode, @date, @time, @referance_id, @activity_hash, @duration)",
				gameid, mode, date, time, ref_id, activity_hash, duration);

			SQLiteCommand command = new SQLiteCommand(sql, connection);

			command.Parameters.AddWithValue("@game_id", gameid);
			command.Parameters.AddWithValue("@mode", mode);
			command.Parameters.AddWithValue("@date", date);
			command.Parameters.AddWithValue("@time", time);
			command.Parameters.AddWithValue("@referance_id", ref_id);
			command.Parameters.AddWithValue("@activity_hash", activity_hash);
			command.Parameters.AddWithValue("@duration", duration);

			command.ExecuteNonQuery();
			return connection.LastInsertRowId;

			//return 0;
		}

		public long InsertPlayer(long membershipId, long membershipType, string displayName, long characterId, long gameId, long timePlayed, long kills, long perKills, long assists, long deaths, long grenade, long melee, long super, long ability)
		{
			string sql = "INSERT INTO player(membership_id, membership_type, display_name, character_id, game_id, time_played, kills,percision_kills , assists, deaths, grenade, melee, super, ability) " +
				"VALUES (@membership_id, @membership_type, @display_name, @character_id, @game_id, @time_played, @kills, @percision_kills, @assists, @deaths, @grenade, @melee, @super, @ability)";

			SQLiteCommand command = new SQLiteCommand(sql, connection);

			command.Parameters.AddWithValue("@membership_id", membershipId);
			command.Parameters.AddWithValue("@membership_type", membershipType);
			command.Parameters.AddWithValue("@display_name", displayName);
			command.Parameters.AddWithValue("@character_id", characterId);
			command.Parameters.AddWithValue("@game_id", gameId);
			command.Parameters.AddWithValue("@time_played", timePlayed);
			command.Parameters.AddWithValue("@kills", kills);
			command.Parameters.AddWithValue("@percision_kills", perKills);
			command.Parameters.AddWithValue("@assists", assists);
			command.Parameters.AddWithValue("@deaths", deaths);
			command.Parameters.AddWithValue("@grenade", grenade);
			command.Parameters.AddWithValue("@melee", melee);
			command.Parameters.AddWithValue("@super", super);
			command.Parameters.AddWithValue("@ability", ability);

			command.ExecuteNonQuery();
			return connection.LastInsertRowId;
		}

		public long InsertWeapon(long gunId, long kills, long pKills, long playerId)
		{
			string sql = string.Format("INSERT INTO gun(gun_id, kills, percision_kills, player_id) VALUES ({0},{1},{2},{3})",
				   gunId, kills, pKills, playerId);

			SQLiteCommand command = new SQLiteCommand(sql, connection);
			command.ExecuteNonQuery();
			return connection.LastInsertRowId;
		}

		public long InsertPGCRList(List<PGCR> games)
		{
			using (var cmd = new SQLiteCommand(connection))
			{
				using (var transaction = connection.BeginTransaction())
				{
					foreach (var item in games)
					{
						InsertPGCR(item);
					}
					transaction.Commit();
				}
			}
			return 0;
		}

		public long InsertPGCR(PGCR pgcr)
		{
			if (pgcr.ErrorStatus != "Success")
			{
				Console.WriteLine("*ErrorStatus:" + pgcr.ErrorStatus);
				return -1;
			}

			if (pgcr.Response == null)
			{
				Console.WriteLine("*No Response");
				return -2;
			}

			if (pgcr.Response.entries == null)
			{
				Console.WriteLine("*No Entries:");
				return -3;
			}

			if (pgcr.Response.entries[0].extended == null)
			{
				Console.WriteLine("*No Extended");
				return -4;
			}

			long game_id = long.Parse(pgcr.Response.activityDetails.instanceId);
			long mode = (long)pgcr.Response.activityDetails.mode;
			string date_time = pgcr.Response.period;
			string date = date_time.Split('T')[0];
			string time = date_time.Split('T')[1];
			long ref_id = pgcr.Response.activityDetails.referenceId;
			long activity_hash = pgcr.Response.activityDetails.directorActivityHash;
			long duration = 0;
			if (pgcr.Response.entries.Count > 0)
				duration = (long)pgcr.Response.entries[0].values["activityDurationSeconds"].basic.value;

			//insert game
			long gId = InsertGame(game_id, mode, date, time, ref_id, activity_hash, duration);

			foreach (var player in pgcr.Response.entries)
			{
				//insert players
				long membership_id = long.Parse(player.player.destinyUserInfo.membershipId);
				long membership_type = player.player.destinyUserInfo.membershipType;
				string display_name = "";
				if (player.player.destinyUserInfo.displayName != null)
					display_name = player.player.destinyUserInfo.displayName;
				long character_id = long.Parse(player.characterId);
				game_id = game_id;
				long time_played = (long)player.values["timePlayedSeconds"].basic.value;
				long kills = (long)player.values["kills"].basic.value;
				long percision_kills = (long)player.extended.values["precisionKills"].basic.value;
				long assists = (long)player.values["assists"].basic.value;
				long deaths = (long)player.values["deaths"].basic.value;
				long grenade = (long)player.extended.values["weaponKillsGrenade"].basic.value;
				long melee = (long)player.extended.values["weaponKillsMelee"].basic.value;
				long super_kills = (long)player.extended.values["weaponKillsSuper"].basic.value;
				long ability = (long)player.extended.values["weaponKillsAbility"].basic.value;

				long playerInsertId = InsertPlayer(membership_id, membership_type, display_name, character_id, game_id, time_played, kills, percision_kills, assists, deaths, grenade, melee, super_kills, ability);

				//test for weapons data
				if (player.extended == null) continue;
				if (player.extended.weapons == null) continue;
				//insert weapons
				foreach (var weapon in player.extended.weapons)
				{
					long gun_id = weapon.referenceId;
					long wkills = (long)weapon.values.uniqueWeaponKills.basic.value;
					long p_kills = (long)weapon.values.uniqueWeaponPrecisionKills.basic.value;
					long player_id = playerInsertId;

					InsertWeapon(gun_id, wkills, p_kills, player_id);
				}
			}
			return 0;
		}

	}
}
