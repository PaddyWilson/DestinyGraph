﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DestinyPGCR
{
    public class PGCR
    {
		public Response Response { get; set; }
		public long ErrorCode { get; set; }
		public long ThrottleSeconds { get; set; }
		public string ErrorStatus { get; set; }
		public string Message { get; set; }
		public MessageData MessageData { get; set; }
	}
	public class ActivityDetails
	{
		public long referenceId { get; set; }
		public long directorActivityHash { get; set; }
		public string instanceId { get; set; }
		public long mode { get; set; }
		public List<long> modes { get; set; }
		public bool isPrivate { get; set; }
	}

	public class MainBasic
	{
		public Basic basic { get; set; }
	}

	public class Basic
	{
		public double value { get; set; }
		public string displayValue { get; set; }
	}

	public class Score
	{
		public Basic basic { get; set; }
	}

	public class DestinyUserInfo
	{
		public string iconPath { get; set; }
		public long membershipType { get; set; }
		public string membershipId { get; set; }
		public string displayName { get; set; }
	}

	public class BungieNetUserInfo
	{
		public string iconPath { get; set; }
		public long membershipType { get; set; }
		public string membershipId { get; set; }
		public string displayName { get; set; }
	}

	public class Player
	{
		public DestinyUserInfo destinyUserInfo { get; set; }
		public string characterClass { get; set; }
		public long classHash { get; set; }
		public long raceHash { get; set; }
		public long genderHash { get; set; }
		public long characterLevel { get; set; }
		public long lightLevel { get; set; }
		public BungieNetUserInfo bungieNetUserInfo { get; set; }
		public long emblemHash { get; set; }
	}

	public class UniqueWeaponKills
	{
		public Basic basic { get; set; }
	}
	public class UniqueWeaponPrecisionKills
	{
		public Basic basic { get; set; }
	}

	public class UniqueWeaponKillsPrecisionKills
	{
		public Basic basic { get; set; }
	}

	public class Values2
	{
		public UniqueWeaponKills uniqueWeaponKills { get; set; }
		public UniqueWeaponPrecisionKills uniqueWeaponPrecisionKills { get; set; }
		public UniqueWeaponKillsPrecisionKills uniqueWeaponKillsPrecisionKills { get; set; }
	}

	public class Weapon
	{
		public long referenceId { get; set; }
		public Values2 values { get; set; }
	}

	public class Extended
	{
		public List<Weapon> weapons { get; set; }
		public Dictionary<string, MainBasic> values { get; set; }
	}

	public class Entry
	{
		public long standing { get; set; }
		public Score score { get; set; }
		public Player player { get; set; }
		public string characterId { get; set; }
		public Dictionary<string, MainBasic> values { get; set; }
		public Extended extended { get; set; }
	}

	public class Response
	{
		public string period { get; set; }
		public long startingPhaseIndex { get; set; }
		public ActivityDetails activityDetails { get; set; }
		public List<Entry> entries { get; set; }
		public List<object> teams { get; set; }
	}

	public class MessageData
	{
	}

}
