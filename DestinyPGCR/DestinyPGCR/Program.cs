﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace DestinyPGCR
{
	public class ThreadInfo
	{
		public TimeSpan time;

		public long got;
		public long gotTotal;
		public long convert;
		public long insert;

		public long error;
		public long errorSuccess;
		public long errorConvert;
		public long reget;

		public void Reset()
		{
			lock (this)
			{
				time = new TimeSpan();

				got = 0;
				convert = 0;
				insert = 0;
				error = 0;
				errorSuccess = 0;
				errorConvert = 0;
				reget = 0;
			}
		}

	}

	class Program
	{
		static string API_KEY = "050bcbfe741f46f6be997949ea9b26cf";
		static string BASE_URL = @"https://www.bungie.net/Platform/Destiny2";

		static string DATABASE_FILE = @"C:\Users\Paddy\Desktop\Destiny\AllGames.db";

		static string PROXIY_FILE = @"C:\Users\Paddy\Desktop\Destiny\proxies.txt";
		static List<string> proxies = new List<string>();

		static HttpClient client;

		static DestinyDB db;
		static CurrentNumber number;

		static bool runningGetThread = true;
		static List<Thread> getGameThreads = new List<Thread>();
		static List<ThreadInfo> threadInfo = new List<ThreadInfo>();

		static bool runningInsertThread = true;
		static ConcurrentQueue<PGCR> insertGameQueue = new ConcurrentQueue<PGCR>();
		static Thread insertThread;

		static System.Timers.Timer infoOutputTimer;

		static void Main(string[] args)
		{
			//setup client
			client = new HttpClient(new HttpClientHandler
			{
				AutomaticDecompression = DecompressionMethods.GZip
									 | DecompressionMethods.Deflate
			});
			client.Timeout = TimeSpan.FromSeconds(30);
			ServicePointManager.DefaultConnectionLimit = 2000;
			client.DefaultRequestHeaders.Add("X-API-Key", API_KEY);
			client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");

			//connect to db
			db = new DestinyDB(DATABASE_FILE);
			db.Connect();

			//get the highest gameid
			long highest = db.GetHighest();
			//set the current pgcr id to get
			number = new CurrentNumber(highest);
			Console.WriteLine("Highest PGCR in DB:" + number.GetCurrent());

			Console.WriteLine("Finding missing");
			{
				List<long> missing = db.FindMissing();
				foreach (var item in missing)
					number.Reget(item);
			}
			Console.WriteLine("Missing " + number.GetQueueCount());

			Console.WriteLine("Starting infoPrint");
			infoOutputTimer = new System.Timers.Timer();
			infoOutputTimer.Interval = 10000;
			infoOutputTimer.Elapsed += new System.Timers.ElapsedEventHandler(InfoThread);
			infoOutputTimer.Enabled = true;
			//infoOutputTimer.Start();

			Console.WriteLine("Starting insert thread");
			insertThread = new Thread(() => { InsertThread(); });
			insertThread.IsBackground = true;
			insertThread.Start();

			//read in proxies from file
			{
				string[] lines = File.ReadAllLines(PROXIY_FILE);
				foreach (var item in lines)
					proxies.Add(item);
			}

			//create request threads for the different proxies and start them
			Console.WriteLine("Starting " + proxies.Count + " get Theads");
			int i = 0;
			foreach (var item in proxies)
			{
				ThreadInfo info = new ThreadInfo();
				threadInfo.Add(info);

				int j = i;
				Thread thread = new Thread(() => { GetPGCRThread(j, item, info); });
				thread.IsBackground = true;
				thread.Start();
				i++;
			}

			//wait for key to quit
			Console.WriteLine("Press key to end program");
			Console.ReadKey();

			runningGetThread = false;

			Console.WriteLine("Waiting for get threads");
			foreach (var item in getGameThreads)
			{
				item.Join();
			}

			Console.WriteLine("Waiting for insert threads");
			Thread.Sleep(3);
			runningInsertThread = false;
			insertThread.Join();
			db.Disconnect();

			infoOutputTimer.Stop();
		}

		static async Task<ReturnPGCRString> MakeRequestAsync(long number)
		{
			var response = await client.GetAsync(BASE_URL + "/Stats/PostGameCarnageReport/" + number + "/");

			ReturnPGCRString output;
			output.pgcr = "";
			output.gameID = number;

			//verify if request was good
			if (response.IsSuccessStatusCode)
			{
				output.pgcr = await response.Content.ReadAsStringAsync();
			}
			return output;
		}

		static async Task<ReturnPGCRString> MakeRequestAsync(string proxy, long number)
		{
			var response = await client.GetAsync(proxy + BASE_URL + "/Stats/PostGameCarnageReport/" + number + "/");

			ReturnPGCRString output;
			output.pgcr = "";
			output.gameID = number;

			//verify if request was good
			if (response.IsSuccessStatusCode)
			{
				output.pgcr = await response.Content.ReadAsStringAsync();
			}
			return output;
		}

		static int getCount = 200;
		static void GetPGCRThread(int threadNumber, string proxy, ThreadInfo info)
		{
			Stopwatch timer = new Stopwatch();
			while (runningGetThread)
			{
				timer.Restart();
				//make request
				List<Task<ReturnPGCRString>> taskList = new List<Task<ReturnPGCRString>>();
				List<long> getting = new List<long>();
				for (int i = 0; i < getCount; i++)
				{
					long j = number.GetNext();
					taskList.Add(MakeRequestAsync(proxy, j));
					getting.Add(j);

					info.got++;
				}

				//might timeout and error
				try
				{
					Task.WaitAll(taskList.ToArray());
				}
				catch (Exception)
				{
					//Console.WriteLine(threadNumber + " Timeout");
					foreach (var item in getting)
					{
						number.Reget(item);
						info.reget++;
					}

					info.error += getting.Count;

					continue;
				}

				//convert to pgcr's
				List<PGCR> pgcr = new List<PGCR>();
				for (int i = 0; i < taskList.Count; i++)
				{
					PGCR game;
					try
					{
						game = JsonConvert.DeserializeObject<PGCR>(taskList[i].Result.pgcr);
					}
					catch (Exception)
					{
						//Console.WriteLine(threadNumber + " Convert error " + taskList[i].Result.gameID);
						number.Reget(taskList[i].Result.gameID);
						info.errorConvert++;
						info.reget++;
						continue;
					}

					if (game == null)
					{
						//Console.WriteLine(threadNumber + " Game NULL " + taskList[i].Result.gameID);
						number.Reget(taskList[i].Result.gameID);
						info.errorConvert++;
						info.reget++;
						continue;
					}

					if (game.ErrorStatus != "Success")
					{
						Console.WriteLine(threadNumber + " ErrorStatus:" + game.ErrorStatus + " " + taskList[i].Result.gameID);
						number.Reget(taskList[i].Result.gameID);
						info.errorSuccess++;
						info.reget++;
						continue;
					}
					else
					{
						pgcr.Add(game);
						info.convert++;
					}
				}

				foreach (var item in pgcr)
				{
					insertGameQueue.Enqueue(item);
					info.insert++;
				}

				info.time = timer.Elapsed;
				//Console.WriteLine(threadNumber + " Time:" + timer.Elapsed.TotalSeconds + " number:" + number.GetCurrent());
				if (timer.Elapsed < TimeSpan.FromSeconds(10))
				{
					//Console.WriteLine(threadNumber + " Sleeping");
					Thread.Sleep(TimeSpan.FromSeconds(10) - timer.Elapsed);
				}
			}
			//do this differtly
			//db.InsertPGCRList(pgcr);
		}

		static void InsertThread()
		{
			while (runningInsertThread)
			{
				Thread.Sleep(TimeSpan.FromSeconds(1));
				while (insertGameQueue.Count != 0)
				{
					Thread.Sleep(TimeSpan.FromSeconds(1));
					//Console.WriteLine("Inserting");
					List<PGCR> insert = new List<PGCR>();

					if (insertGameQueue.Count < 100) continue;

					for (int i = 0; i < 1000; i++)
					{
						PGCR pgcr;

						//no game in queue
						if (!insertGameQueue.TryDequeue(out pgcr))
							break;

						insert.Add(pgcr);
					}

					db.InsertPGCRList(insert);
				}
			}
		}

		static void InfoThread(object source, ElapsedEventArgs e)
		{
			ThreadInfo info = new ThreadInfo();
			double average = 0;//new TimeSpan();
			TimeSpan highest = new TimeSpan();

			foreach (var item in threadInfo)
			{
				//consolodate info
				average += item.time.TotalMilliseconds;

				if (item.time > highest)
					highest = item.time;

				item.gotTotal += item.got;

				info.gotTotal += item.gotTotal;
				info.got += item.got;
				info.convert += item.convert;
				info.insert += item.insert;
				info.error += item.error;
				info.errorSuccess += item.errorSuccess;
				info.errorConvert += item.errorConvert;
				info.reget += item.reget;

			}

			average = average / threadInfo.Count;
			TimeSpan avg = TimeSpan.FromMilliseconds(average);

			Console.WriteLine(DateTime.Now.ToString("h:mm:ss") + " AT:{0} GT:{8,9} Got:{2,4} Time:{3,5} HTime:{10,5} IC:{9,4} QC:{1,4} ER:{4,4} EC:{5,4} ES:{6,4} RE:{7,4}", 
				number.GetCurrent(), number.GetQueueCount(), info.got, Math.Round(avg.TotalSeconds,2), info.error, info.errorConvert, info.errorSuccess, info.reget, info.gotTotal, insertGameQueue.Count, Math.Round(highest.TotalSeconds,2));

			foreach (var item in threadInfo)
			{
				item.Reset();
			}
		}
	}
}
