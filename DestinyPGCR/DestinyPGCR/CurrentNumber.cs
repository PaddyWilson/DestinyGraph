﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestinyPGCR
{
	public class CurrentNumber
	{
		long number = 0;

		ConcurrentQueue<long> queue = new ConcurrentQueue<long>();

		public CurrentNumber(long number)
		{
			this.number = number;
		}

		public long GetNext()
		{
			long j = 0;
			if (queue.TryDequeue(out j))
				return j;

			lock (this)
			{
				number++;
				j = number;
			}
			return j;
		}

		public long GetCurrent()
		{
			return number;
		}

		public void SetNumber(long num)
		{
			number = num;
		}

		public void Reget(long number)
		{
			queue.Enqueue(number);
		}

		public long GetQueueCount() { return queue.Count; }
	}
}
