﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestinyPGCR
{
	/// <summary>
	/// the data return from MakeRequestAsync()
	/// </summary>
	struct ReturnPGCRString
	{
		public long gameID;
		public string pgcr;
	}
}
