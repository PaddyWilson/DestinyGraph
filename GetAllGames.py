import requests
import json
import time, datetime
from subprocess import Popen

import asyncio, aiofiles, aiohttp

import sqlite3

apiKey = '050bcbfe741f46f6be997949ea9b26cf'
playerName = 'padman wilson'

proxies =[
    '',
    'https://dry-fjord-24305.herokuapp.com/',
    'https://morning-earth-30433.herokuapp.com/',
    'https://serene-plains-93879.herokuapp.com/',
    'https://tranquil-reaches-24461.herokuapp.com/',
    'https://warm-taiga-69561.herokuapp.com/',
    'https://vast-refuge-40847.herokuapp.com/',
    'https://vast-sierra-67835.herokuapp.com/',
    'https://infinite-sea-97465.herokuapp.com/',
    'https://young-meadow-19351.herokuapp.com/',
    'https://young-river-38345.herokuapp.com/',
    'https://enigmatic-river-72096.herokuapp.com/',
    'https://frozen-oasis-52207.herokuapp.com/',
    'https://intense-tundra-97396.herokuapp.com/',
    'https://tranquil-basin-23423.herokuapp.com/',
    'https://young-coast-34477.herokuapp.com/',
    'https://damp-woodland-13429.herokuapp.com/',
    'https://murmuring-garden-47085.herokuapp.com/',
    'https://rocky-beyond-78244.herokuapp.com/',
    'https://serene-scrubland-34223.herokuapp.com/',
    'https://stark-bayou-55073.herokuapp.com/'
]

HEADER = {
    "X-API-KEY": apiKey,
    "Accept-Encoding": "gzip, deflate"
}
BASE_URL = 'https://www.bungie.net/Platform/Destiny2'

DB_NAME = 'AllGames.db'

# create database with CreateDatabase.py
p = Popen("python CreateDatabase.py " + DB_NAME, shell=True)
p.wait()

GAME_SQL = 'INSERT INTO game(game_id, mode, date, time, referance_id, activity_hash, duration) VALUES (?,?,?,?,?,?,?)'
PLAYER_SQL = 'INSERT INTO player(membership_id, membership_type, display_name, character_id, game_id, time_played, kills,percision_kills , assists, deaths, grenade, melee, super, ability) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
GUN_SQL = 'INSERT INTO gun(gun_id, kills, percision_kills, player_id) VALUES (?,?,?,?)'

GET_COUNT = 'SELECT MAX(game_id) FROM game'

def saveToFile(filename, text):
    file = open(filename, "a")
    file.write(text+'\n')
    file.close()

def readJsonFile(filename):
    return json.loads(open(filename).read())

def makeRequest(url):
    return requests.get(url, headers=HEADER)

session = aiohttp.ClientSession()
async def makeRequestAsync(url):
    data = {}
    # async with aiohttp.ClientSession() as session:
    async with session.get(url, headers=HEADER) as resp:
        data = await resp.text()
    return json.loads(data)

def getPGCR(gameId):
    output = makeRequest(BASE_URL+'/Stats/PostGameCarnageReport/'+str(gameId)+'/')
    print(output.text)
    return output.json()

async def getPGCRAsync(gameId):
    endpoint = '/Stats/PostGameCarnageReport/'+str(gameId)+'/'
    url = f'{BASE_URL}{endpoint}'
    data = {}
    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=HEADER) as resp:
            data = await resp.text()
    # print(data)
    return json.loads(data)

def getCount(c):
    c.execute(GET_COUNT)
    rows = c.fetchall()
    # print(rows[0][0])
    return rows[0][0]

def insertGame(c, data):
    game_id = data['Response']['activityDetails']['instanceId']
    mode = data['Response']['activityDetails']['mode']
    date_time = data['Response']['period']
    date = date_time.split('T')[0]
    time = date_time.split('T')[1]
    ref_id = data['Response']['activityDetails']['referenceId']
    activity_hash = data['Response']['activityDetails']['directorActivityHash']
    duration = 0
    if(len(data['Response']['entries']) > 0):
        # print('time')
        duration = data['Response']['entries'][0]['values']['activityDurationSeconds']['basic']['value']
    
    try:
        c.execute(GAME_SQL, (game_id, mode, date, time, ref_id, activity_hash, duration))
    except sqlite3.IntegrityError:
        print('insert into "game" ERROR ' + str(game_id))
        saveToFile('db.txt', ('insert into "game" ERROR ' + str(game_id)))
        # saveToFile("PGCR/"+str(game_id)+".json", json.dumps(data))

    return game_id

def insertPlayer(c, player, game_id):
    membership_id = player['player']['destinyUserInfo']['membershipId']
    membership_type = player['player']['destinyUserInfo']['membershipType']
    display_name = ''
    if('displayName' in player['player']['destinyUserInfo']):
        display_name = player['player']['destinyUserInfo']['displayName']
    character_id = player['characterId']
    game_id = game_id
    time_played = player['values']['timePlayedSeconds']['basic']['value']
    kills = player['values']['kills']['basic']['value']
    percision_kills = player['extended']['values']['precisionKills']['basic']['value']
    assists = player['values']['assists']['basic']['value']
    deaths = player['values']['deaths']['basic']['value']
    grenade = player['extended']['values']['weaponKillsGrenade']['basic']['value']
    melee = player['extended']['values']['weaponKillsMelee']['basic']['value']
    super_kills = player['extended']['values']['weaponKillsSuper']['basic']['value']
    ability = player['extended']['values']['weaponKillsAbility']['basic']['value']

    try:
        c.execute(PLAYER_SQL, (membership_id, membership_type, display_name, character_id, game_id,
                               time_played, kills, percision_kills, assists, deaths, grenade, melee, super_kills, ability))
    except sqlite3.IntegrityError:
        print('insert into "player" ERROR')

    return c.lastrowid

def insertGun(c, gun, player_id):
    gun_id = gun['referenceId']
    kills = gun['values']['uniqueWeaponKills']['basic']['value']
    p_kills = gun['values']['uniqueWeaponPrecisionKills']['basic']['value']
    player_id = player_id
    try:
        c.execute(GUN_SQL, (gun_id, kills, p_kills, player_id))
    except sqlite3.IntegrityError:
        print('gun ERROR')
    return c.lastrowid

def getGamesAndInsert(getCount):
    gameDataList = loop.run_until_complete(
        asyncio.gather(
            *(getPGCRAsync(arg) for arg in getCount)
        )
    )
    end1 = time.time()
    # print('Time:' + str(end1 - start))
    # print('Inserting')
    for data in gameDataList:
        # enter into database
        if(data['ErrorStatus'] != 'Success'):
            print('*ErrorStatus' + data['ErrorStatus'])
            saveToFile('nope.txt', str(game))
            continue

        if('Response' not in data):
            print('*no extended 1' + game)
            saveToFile('nope.txt', str(game))
            continue

        if('entries' not in data['Response']):
            print('*no extended 2' + game)
            saveToFile('nope.txt', str(game))
            continue

        if('extended' not in data['Response']['entries'][0]):
            print('*no extended 3' + game)
            saveToFile('nope.txt', str(game))
            continue

        game_id = insertGame(c, data)
        for player in data['Response']['entries']:
            table_id = insertPlayer(c, player, game_id)

            if('extended' in player):
                if('weapons' in player['extended']):
                    for gun in player['extended']['weapons']:
                        gun_id = insertGun(c, gun, table_id)
    
    if (gotCount%(amount*4) == 0):
        # print('commit')
        conn.commit()
    end = time.time()
    print('Execute Time:' + str(end - start) + ' Get Time:' + str(end1 - start))
    # end of insert data
    if((end - start) < 1):
        # print('Sleep')
        time.sleep(1-(end - start))

# connect to db
conn = sqlite3.connect(DB_NAME)
c = conn.cursor()
loop = asyncio.get_event_loop()

print('Getting Count')

gotCount = getCount(c)

if(str(gotCount) == "None"):
    gotCount = 0

print('Starting at:' + str(gotCount))
game = 'nope'
print('Getting PGCRs')

amount = 20 * len(proxies)
timer = time.time()
someCount = 0
while(True):
    print('PGCR:' + str(gotCount))
    start = time.time()
    getCount = []

    for u in proxies:
        for i in range(amount):
            # print(gotCount)
            gotCount += 1
            someCount += 1
            url = f'{u}{BASE_URL}/Stats/PostGameCarnageReport/{str(gotCount)}/'
            # print(str(gotCount))
            getCount.append(url)

    running = True
    gameDataList = []
    while(running):
        try:
            print('Getting:'+str(len(getCount)))
            gameDataList = loop.run_until_complete(
                asyncio.gather(
                    *(makeRequestAsync(arg) for arg in getCount)
                )
            )
            running = False
        except:
            print('Getting error')

    end1 = time.time()

    # print('Inserting')
    start1 = time.time()
    for data in gameDataList:
        # enter into database
        if(data['ErrorStatus'] != 'Success'):
            print('*ErrorStatus' + data['ErrorStatus'])
            saveToFile('nope.txt', '*ErrorStatus' + data['ErrorStatus'])
            continue

        if('Response' not in data):
            print('*no extended 1' + game)
            saveToFile('nope.txt', '*no extended 1')
            continue

        if('entries' not in data['Response']):
            print('*no extended 2' + game)
            saveToFile('nope.txt', '*no extended 2')
            continue

        if('extended' not in data['Response']['entries'][0]):
            print('*no extended 3' + game)
            saveToFile('nope.txt', '*no extended 3')
            continue

        game_id = insertGame(c, data)

        for player in data['Response']['entries']:
            table_id = insertPlayer(c, player, game_id)

            if('extended' in player):
                if('weapons' in player['extended']):
                    for gun in player['extended']['weapons']:
                        gun_id = insertGun(c, gun, table_id)
    
    # if (gotCount%(amount*1) == 0):
        # print('commit')
    conn.commit()
    end = time.time()
    print('Execute:' + '%.1f'%(end - start) + ' Get:' + '%.1f'%(end1 - start) + ' In:' + '%.1f'%(end - start1) + ' Total:'+'%.1f'%(time.time() - timer) + ' C:'+str(someCount))
    # end of insert data
    if((end - start) < 1):
        # print('Sleep')
        time.sleep(1.5-(end - start))

conn.commit()
conn.close()

print('Done')