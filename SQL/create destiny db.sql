BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS 'player' (
	'id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	'membership_id'	INTEGER NOT NULL,
	'membership_type'	INTEGER NOT NULL,
	'display_name'	TEXT,
	'character_id'	INTEGER NOT NULL,
	'game_id'	INTEGER NOT NULL,
	'time_played'	INTEGER NOT NULL,
	'kills'	INTEGER,
	'percision_kills'	INTEGER,
	'assists'	INTEGER,
	'deaths'	INTEGER,
	'grenade'	INTEGER,
	'melee'	INTEGER,
	'super'	INTEGER,
	'ability'	INTEGER,
	FOREIGN KEY('game_id') REFERENCES 'game'('game_id')
);
CREATE TABLE IF NOT EXISTS 'gun' (
	'id'	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	'gun_id'	INTEGER NOT NULL,
	'kills'	INTEGER NOT NULL,
	'percision_kills'	INTEGER NOT NULL,
	'player_id'	INTEGER NOT NULL,
	FOREIGN KEY('player_id') REFERENCES 'player'('id')
);
CREATE TABLE IF NOT EXISTS 'game' (
	'game_id'	INTEGER NOT NULL,
	'mode'	INTEGER NOT NULL,
	'date'	TEXT NOT NULL,
	'time'	TEXT NOT NULL,
	'referance_id'	INTEGER NOT NULL,
	'activity_hash'	INTEGER NOT NULL,
	'duration'	INTEGER NOT NULL,
	PRIMARY KEY('game_id')
);

CREATE TABLE IF NOT EXISTS 'modes' (
	'name'	TEXT NOT NULL,
	'mode'	INTEGER NOT NULL
);

INSERT INTO 'modes' VALUES ('Reserved13',13);
INSERT INTO 'modes' VALUES ('CrimsonDoubles',15);
INSERT INTO 'modes' VALUES ('Nightfall',16);
INSERT INTO 'modes' VALUES ('HeroicNightfall',17);
INSERT INTO 'modes' VALUES ('AllStrikes',18);
INSERT INTO 'modes' VALUES ('IronBanner',19);
INSERT INTO 'modes' VALUES ('Reserved20',20);
INSERT INTO 'modes' VALUES ('Reserved21',21);
INSERT INTO 'modes' VALUES ('Reserved22',22);
INSERT INTO 'modes' VALUES ('Reserved24',24);
INSERT INTO 'modes' VALUES ('AllMayhem',25);
INSERT INTO 'modes' VALUES ('Reserved26',26);
INSERT INTO 'modes' VALUES ('Reserved27',27);
INSERT INTO 'modes' VALUES ('Reserved28',28);
INSERT INTO 'modes' VALUES ('Reserved29',29);
INSERT INTO 'modes' VALUES ('Reserved30',30);
INSERT INTO 'modes' VALUES ('Supremacy',31);
INSERT INTO 'modes' VALUES ('PrivateMatchesAll',32);
INSERT INTO 'modes' VALUES ('Survival',37);
INSERT INTO 'modes' VALUES ('Countdown',38);
INSERT INTO 'modes' VALUES ('TrialsOfTheNine',39);
INSERT INTO 'modes' VALUES ('Social',40);
INSERT INTO 'modes' VALUES ('TrialsCountdown',41);
INSERT INTO 'modes' VALUES ('TrialsSurvival',42);
INSERT INTO 'modes' VALUES ('IronBannerControl',43);
INSERT INTO 'modes' VALUES ('IronBannerClash',44);
INSERT INTO 'modes' VALUES ('IronBannerSupremacy',45);
INSERT INTO 'modes' VALUES ('ScoredNightfall',46);
INSERT INTO 'modes' VALUES ('ScoredHeroicNightfall',47);
INSERT INTO 'modes' VALUES ('Rumble',48);
INSERT INTO 'modes' VALUES ('AllDoubles',49);
INSERT INTO 'modes' VALUES ('Doubles',50);
INSERT INTO 'modes' VALUES ('PrivateMatchesClash',51);
INSERT INTO 'modes' VALUES ('PrivateMatchesControl',52);
INSERT INTO 'modes' VALUES ('PrivateMatchesSupremacy',53);
INSERT INTO 'modes' VALUES ('PrivateMatchesCountdown',54);
INSERT INTO 'modes' VALUES ('PrivateMatchesSurvival',55);
INSERT INTO 'modes' VALUES ('PrivateMatchesMayhem',56);
INSERT INTO 'modes' VALUES ('PrivateMatchesRumble',57);
INSERT INTO 'modes' VALUES ('HeroicAdventure',58);
INSERT INTO 'modes' VALUES ('Showdown',59);

COMMIT;