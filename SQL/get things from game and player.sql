SELECT 
	game.date, 
	count(game.date),
	sum(player.time_played)/60 as "time",
	sum(player.kills), 
	sum(player.percision_kills),
	sum(player.assists),
	sum(player.deaths),
	sum(player.grenade),
	sum(player.melee),
	sum(player.super),
	sum(player.ability)
FROM player
join game on player.game_id=game.game_id
WHERE player.membership_id=4611686018430110158
	--(SELECT player.membership_id FROM game 
		--where player.display_name='padman wilson' COLLATE NOCASE and player.membership_type=1 LIMIT 1)
group by game.date;