SELECT DISTINCT game_id +1 as missing
FROM game
WHERE game_id + 1 NOT IN (SELECT DISTINCT game_id FROM game);
-- last row returned is above whats in the db