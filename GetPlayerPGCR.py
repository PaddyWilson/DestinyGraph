import requests
import json
import time, datetime, sys
from pathlib import Path
import Queue
# async things
import asyncio, aiofiles, aiohttp

import sqlite3

apiKey = '050bcbfe741f46f6be997949ea9b26cf'

# # GET THESE VIA API
PLAYER_NAME = 'padman wilson'
# MEMBERSHIP_TYPE = '0' # none 
MEMBERSHIP_TYPE = '1' # xbox 
# MEMBERSHIP_TYPE = '2' # playstation
# MEMBERSHIP_TYPE = '4' # pc
MEMBERSHIP_ID = '4611686018430110158'#paddy
# MEMBERSHIP_ID = '4611686018467294153'#paddy pc
# MEMBERSHIP_ID = '4611686018430454381'#nat
# MEMBERSHIP_ID = '4611686018430377564'#evan
# MEMBERSHIP_ID = '4611686018430414388'#malcom
# MEMBERSHIP_ID = '4611686018469734397'#malcom dad

# MEMBERSHIP_ID = '4611686018467185358'#goth pc
# MEMBERSHIP_ID = '4611686018428388122'#goth play
# MEMBERSHIP_ID = '4611686018429570369'#goth xbox
# MEMBERSHIP_ID = '4611686018436326468'#jannicck play
# MEMBERSHIP_ID = '4611686018431043209'

# cmd args
if len(sys.argv) > 1:
    print('args:' + sys.argv[1]+','+sys.argv[2] +','+sys.argv[3])
    MEMBERSHIP_ID = sys.argv[1]
    MEMBERSHIP_TYPE = sys.argv[2]
    PLAYER_NAME = sys.argv[3]

HEADER = {"X-API-KEY": apiKey}
BASE_URL = 'https://www.bungie.net/Platform/Destiny2'

DB_NAME = 'player.db'
GAME_SQL = 'INSERT INTO game(game_id, mode, date, time, referance_id, activity_hash, duration) VALUES (?,?,?,?,?,?,?)'
PLAYER_SQL = """INSERT INTO 
    player(membership_id, membership_type, display_name, character_id, game_id, time_played, kills,percision_kills , assists, deaths, grenade, melee, super, ability) 
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    """
GUN_SQL = 'INSERT INTO gun(gun_id, kills, percision_kills, player_id) VALUES (?,?,?,?)'

FIND_GAMES = """
    SELECT game.game_id FROM game
    JOIN player ON player.game_id=game.game_id
    WHERE player.membership_id=?;
    """    
FIND_LAST_GAME_ID = 'SELECT MAX(player.game_id) FROM player WHERE player.membership_id=? AND player.character_id=?;'


def saveToFile(filename, text):
    file = open(filename, "w")
    file.write(json.dumps(text))
    file.close()

def readJsonFile(filename):
    return json.loads(open(filename).read())

def makeRequest(url):
    return requests.get(url, headers=HEADER)

def getProfile(membershipType, membershipId):
    output = makeRequest(BASE_URL+'/'+str(membershipType) +
                         '/Profile/'+str(membershipId)+'/?components=100')
    # saveToFile("getProfile.json", output.json())
    return output.json()

def getActivityHistory(membershipType, membershipId, characterId, count, page):    
    output = makeRequest(BASE_URL + '/' + membershipType + '/Account/' + membershipId + '/Character/' +
                         characterId + '/Stats/Activities/?mode=0&count=' + str(count) + '&page=' + str(page))
    return output.json()

def getPGCR(gameId):
    output = makeRequest(BASE_URL+'/Stats/PostGameCarnageReport/'+gameId+'/')
    # saveToFile("PGCR/"+str(gameId)+".json", output.json())
    return output.json()

# use this one
async def getPGCRAsync(gameId):
    endpoint = '/Stats/PostGameCarnageReport/'+str(gameId)+'/'
    url = f'{BASE_URL}{endpoint}'
    # print('Getting all reports...')
    data = {}
    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=HEADER) as resp:
            data = await resp.json()
    # saveToFile("PGCR/"+str(gameId)+".json", data)
    return data

def insertGame(c, data):
    game_id = data['Response']['activityDetails']['instanceId']
    mode = data['Response']['activityDetails']['mode']
    date_time = data['Response']['period']
    date = date_time.split('T')[0]
    time = date_time.split('T')[1]
    ref_id = data['Response']['activityDetails']['referenceId']
    activity_hash = data['Response']['activityDetails']['directorActivityHash']
    duration = 0
    if(len(data['Response']['entries']) > 0):
        duration = data['Response']['entries'][0]['values']['activityDurationSeconds']['basic']['value']

    good = True
    try:
        c.execute(GAME_SQL, (game_id, mode, date, time, ref_id, activity_hash, duration))
    except sqlite3.IntegrityError:
        # print('insert into "game" ERROR')
        good = False

    return (game_id, good)

def insertPlayer(c, player, game_id):
    membership_id = player['player']['destinyUserInfo']['membershipId']
    membership_type = player['player']['destinyUserInfo']['membershipType']
    display_name = ''
    if('displayName' in player['player']['destinyUserInfo']):
        display_name = player['player']['destinyUserInfo']['displayName']
    character_id = player['characterId']
    game_id = game_id
    time_played = player['values']['timePlayedSeconds']['basic']['value']
    kills = player['values']['kills']['basic']['value']
    percision_kills = player['extended']['values']['precisionKills']['basic']['value']
    assists = player['values']['assists']['basic']['value']
    deaths = player['values']['deaths']['basic']['value']
    grenade = player['extended']['values']['weaponKillsGrenade']['basic']['value']
    melee = player['extended']['values']['weaponKillsMelee']['basic']['value']
    super_kills = player['extended']['values']['weaponKillsSuper']['basic']['value']
    ability = player['extended']['values']['weaponKillsAbility']['basic']['value']

    try:
        c.execute(PLAYER_SQL, (membership_id, membership_type, display_name, character_id, game_id,
                               time_played, kills, percision_kills, assists, deaths, grenade, melee, super_kills, ability))
    except sqlite3.IntegrityError:
        print('insert into "player" ERROR')

    return c.lastrowid

def insertGun(c, gun, player_id):
    gun_id = gun['referenceId']
    kills = gun['values']['uniqueWeaponKills']['basic']['value']
    p_kills = gun['values']['uniqueWeaponPrecisionKills']['basic']['value']
    player_id = player_id
    try:
        c.execute(GUN_SQL, (gun_id, kills, p_kills, player_id))
    except sqlite3.IntegrityError:
        print('gun ERROR')
    return c.lastrowid

def getGamesFromDB(c, membershipID, gameList):
    returnList = Queue.Queue()
    rows = []
    c.execute(FIND_GAMES, (membershipID, ))
    r = c.fetchall()

    for g in r:
        rows.append(g[0])
    
    for game in gameList:
        if (int(game) not in rows):
            returnList.enqueue(game)

    return returnList

def getNewestGameID(c, membershipID, char_id):
    c.execute(FIND_LAST_GAME_ID, (membershipID, char_id,))
    r = c.fetchall()
    return r[0][0]

# 1=xbox, ?=playstation
membershipId = MEMBERSHIP_ID
membershipType = MEMBERSHIP_TYPE
characterIds = []

gameIds = []#Queue.Queue()

print('Get Profile:' + membershipId + " Name:" + PLAYER_NAME)
data = getProfile(membershipType, membershipId)
characterIds = data['Response']['profile']['data']['characterIds']

# connect to db
conn = sqlite3.connect(DB_NAME)
c = conn.cursor()

for char in characterIds:
    print('Char id:'+char)

print('Getting History')
for characterId in characterIds:    
    print('Getting for character:' + characterId)

    running = True
    count = 100
    page = 0
    gotCount = count
    totalCount = 0
    while(gotCount == count and running):
        print('Getting page:'+str(page))
        try:
            data = getActivityHistory(membershipType, membershipId, characterId, count, page)
            # add each id to a list
            for game in data['Response']['activities']:
                lGameID = game['activityDetails']['instanceId']
                gameIds.append(str(lGameID))
            # end for loop
            gotCount = len(data['Response']['activities'])
            totalCount += gotCount
            if(gotCount == count):
                page += 1
            print('Got '+str(totalCount))
        except:
            gotCount = 0
            print('No game data found')
    # end while loop
# end for char in chars loop
            
print('Total PGCR:'+str(len(gameIds)))

gameList2 = getGamesFromDB(c, membershipId, gameIds)

print('Have:' + str(len(gameIds) - gameList2.size()))

gotCount = 0
print('Getting PGCRs')
# data = getPGCR(gameIds[0])
# enter into database
gameCount = gameList2.size()
loop = asyncio.get_event_loop()
while(gameList2.size() != 0):
    gameList = []

    rangeCount = 22
    if(gameList2.size() < rangeCount):
        rangeCount = gameList2.size()
    for i in range(rangeCount):
        gotCount += 1
        gameList.append(gameList2.dequeue())

    print('Getting games:' + str(gotCount) + '/' + str(gameCount))
    
    start = time.time()
    running = True
    while(running):
        try:
            gameDataList = loop.run_until_complete(
                asyncio.gather(
                    *(getPGCRAsync(arg) for arg in gameList)
                )
            )
            running = False
        except:
            print('Get Error')

    for data in gameDataList:
        # enter into database
        if(data['ErrorStatus'] != 'Success'):
            print('*ErrorStatus' + data['ErrorStatus'])
            saveToFile('nope.txt', str(game))
            continue

        if('Response' not in data):
            print('*no extended 1' + game)
            saveToFile('nope.txt', str(game))
            continue

        if('entries' not in data['Response']):
            print('*no extended 2' + game)
            saveToFile('nope.txt', str(game))
            continue

        if('extended' not in data['Response']['entries'][0]):
            print('*no extended 3' + game)
            saveToFile('nope.txt', str(game))
            continue

        game_id = insertGame(c, data)

        if(game_id[1]==False):
            continue

        game_id=game_id[0]

        for player in data['Response']['entries']:
            table_id = insertPlayer(c, player, game_id)

            if('extended' in player):
                if('weapons' in player['extended']):
                    for gun in player['extended']['weapons']:
                        gun_id = insertGun(c, gun, table_id)
    conn.commit()
    end = time.time()
    print('Execute Time:' + str(end - start))
    # end of insert data

    if((end - start) < 1):
        # print('Sleep')
        time.sleep(1-(end - start))

conn.commit()
conn.close()

print('Done')
