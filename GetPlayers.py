from subprocess import Popen
import sys

players = [
    ['4611686018430110158','1','padman wilson'],# paddy
    ['4611686018467294153','4','padmanwilson'],# paddy pc
    ['4611686018430454381','1','nr scarecrow'],# nat
    ['4611686018430377564','1','devan55'],# evan
    ['4611686018430414388','1','gahudrid13'],# malcom
    ['4611686018469734397','1','menobody']# malcom dad

    # ['4611686018467185358','4','gothalion'],# goth pc
    # ['4611686018428388122','2','gothalion'],# goth play
    # ['4611686018429570369','1','gothalion'],# goth xbox

    # ['4611686018467259109','4','Professorbroman'],# broman pc
    # ['4611686018428397359','2','Professorbroman'],# broman play
    # ['4611686018440003239','1','ProfessorBroman'], # broman xbox    

    # ['4611686018467184568','4','TheTeawrex'],# Teawrex pc
    # ['4611686018433099663','2','TheTeawrex'],# Teawrex play
    # # ['4611686018440003239','1','TheTeawrex'], # Teawrex xbox   

    # ['4611686018467187593','4','Char'],# char pc
    # ['4611686018434922875','2','Charionna'],# char play
    # # ['4611686018440003239','1','Charionna'], # char xbox 

    # ['4611686018439611942','1','CautionIkaBites'], # other guy from clan

    # ['4611686018436326468','2','jannicck'],# jannicck play
    # ['4611686018431043209','1','RudeNo mercy'] # some guy?
]

OPEN_CHART_AFTER_CREATING = False

# create db
p = Popen("python CreateDatabase.py", shell=True)
p.wait()

# get game reports on put into db
filename = 'GetPlayerPGCR.py'
for item in players:
    run = filename + ' {} {} "{}"'.format(item[0], item[1], item[2])
    print("\nStarting " + run)
    p = Popen("python " + run, shell=True)
    p.wait()

# get info from db and put into chart
filename = 'CreateChart.py'
for item in players:
    run = filename + ' {} {} "{}" {}'.format(item[0], item[1], item[2], OPEN_CHART_AFTER_CREATING)
    print("\nStarting " + run)
    p = Popen("python " + run, shell=True)
    p.wait()
