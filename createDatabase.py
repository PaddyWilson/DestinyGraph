import os.path
import sqlite3
import sys
from sqlite3 import Error

DB_NAME = "player.db"
DB_CREATE_SCRIPT = "SQL/create destiny db.sql"

if len(sys.argv) > 1:
    DB_NAME = sys.argv[1]
    print('args:'+DB_NAME)

if os.path.isfile(DB_NAME):
    print("Database exists. Not creating")
    exit()

print("Creating Database")

# create db file
conn = sqlite3.connect(DB_NAME)
print(sqlite3.version)

# read ing create script
createScript = open(DB_CREATE_SCRIPT).read()

# execute script
c = conn.cursor()
c.executescript(createScript)

conn.close()