import requests
import json
import os
import time
import datetime
import sys

import asyncio
import aiofiles
import aiohttp

import sqlite3

apiKey = '050bcbfe741f46f6be997949ea9b26cf'
membershipId = "4611686018430110158"
display_name = 'padman wilson'
platform = '1'
openFile = False

# cmd args
if len(sys.argv) > 1:
    membershipId = sys.argv[1]
    platform = sys.argv[2]
    display_name = sys.argv[3]
    openFile = sys.argv[4]
    print('args:'+membershipId+","+display_name + ","+platform+","+openFile)


DB_NAME = 'player.db'

GET_GAMES = """
SELECT 
	game.date, 
	count(game.date),
	sum(player.time_played)/60,
	sum(player.kills), 
	sum(player.percision_kills),
	sum(player.assists),
	sum(player.deaths),
	sum(player.grenade),
	sum(player.melee),
	sum(player.super),
	sum(player.ability)
FROM player
join game on player.game_id=game.game_id
WHERE player.membership_id=?	
group by game.date;
"""

GET_COUNT = 'SELECT MAX(game_id) FROM game'

def saveToFile(filename, text):
    file = open(filename, "w")
    file.write(text+'\n')
    file.close()


def readJsonFile(filename):
    return json.loads(open(filename).read())


def getCount(c):
    c.execute(GET_COUNT)
    rows = c.fetchall()
    # print(rows[0][0])
    return rows[0][0]


def getPlayerData(c, membershipId):
    c.execute(GET_GAMES, (membershipId,))
    rows = c.fetchall()
    return rows


# connect to db
conn = sqlite3.connect(DB_NAME)
c = conn.cursor()

print('Get Player Data :' + display_name)
alldata = getPlayerData(c, membershipId)

conn.commit()
conn.close()

print('Count:'+str(len(alldata)))

print('Formating data')

count = len(alldata[0])
output = []

for i in range(count):
    output.append([])

index = 0
for data in alldata:
    x = 0
    for g in data:
        output[x].append(g)
        x += 1
    index += 1

print('Generating html')

names = [
    'date',
    'games played',
    'time',
    'kills',
    'percision kills',
    'assists',
    'deaths',
    'grenade',
    'melee',
    'super',
    'ability'
]

colors = [
    '#3e95cd',
    '#8e5ea2',
    '#3cba9f',
    '#e8c3b9',
    '#c45850',
    '#53c450',
    '#4bd1cc',
    '#c62bd1',
    '#ff0000',
    '#ff5400',
    '#8e6f5f'
]

outputHTML = """<html><head><script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script></head>
<body><div style="width:75%;"><canvas id="myChart" width="400" height="400"></canvas></div>  
    <script>/* HOT PATCH GetElementsAtEvent */
        Chart.Controller.prototype.getElementsAtEvent = function (e) {
            var helpers = Chart.helpers;
            var eventPosition = helpers.getRelativePosition(e, this.chart);
            var elementsArray = [];

            var found = (function () {
                if (this.data.datasets) {
                    for (var i = 0; i < this.data.datasets.length; i++) {
                        if (helpers.isDatasetVisible(this.data.datasets[i])) {
                            for (var j = 0; j < this.data.datasets[i].metaData.length; j++) {
                                if (this.data.datasets[i].metaData[j].inLabelRange(eventPosition.x, eventPosition.y)) {
                                    return this.data.datasets[i].metaData[j];
                                }
                            }
                        }
                    }
                }
            }).call(this);

            if (!found) {
                return elementsArray;
            }

            helpers.each(this.data.datasets, function (dataset, dsIndex) {
                if (helpers.isDatasetVisible(dataset)) {
                    elementsArray.push(dataset.metaData[found._index]);
                }
            });

            return elementsArray;
        };
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: """

outputHTML += str(output[0])+",\ndatasets: ["

for i in range(1, len(output)):
    f = '{data:' + str(output[i])+',\n'
    f += 'label:"' + names[i] + '",\n'
    f += 'borderColor:"' + colors[i] + '",\n'
    f += 'pointBackgroundColor:"' + colors[i] + '",\n'
    f += 'fill: true,\n'
    f += 'showLine: true}'
    if(i < (len(output)-1)):
        f += ','
    outputHTML += f

outputHTML += """
] },\noptions: {
                responsive:true,
                scales: {
                    xAxes: [{
                        ticks: {
                            autoSkip: false, maxRotation: 90, minRotation: 90
                        }
                    }]
                },
                tooltips: {
                    mode: 'label'
                },
                hove:{
                    mode:'label'
                }
            }
        });</script>
</body>

</html>"""

oFilename = display_name.replace(" ", "")+platform+'.html'
saveToFile("GeneratedCharts/"+oFilename, outputHTML)

if(str(openFile) == 'True'):
    os.system("start "+oFilename)
print('Done')
