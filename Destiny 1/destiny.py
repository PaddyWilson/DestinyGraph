import requests
import re
import time
import json
import calendar
import sched
import datetime
from pathlib import Path

import sqlite3

apiKey = '050bcbfe741f46f6be997949ea9b26cf'
playerName = 'padman wilson'

HEADER = {"X-API-KEY": apiKey}
BASE_URL = 'https://www.bungie.net/d1/Platform/Destiny'


def saveToFile(filename, text):
    file = open(filename, "w")
    file.write(json.dumps(text))
    file.close()


def readJsonFile(filename):
    return json.loads(open(filename).read())


def makeRequest(url):
    return requests.get(url, headers=HEADER)


def searchForPlayer(displayname, membershipType):
    output = makeRequest(BASE_URL+'/SearchDestinyPlayer/' +
                         membershipType+'/'+displayname+'/')
    # print(output.json()['ErrorStatus'])
    # saveToFile("searchForPlayer.json", output.json())
    return output.json()


def accountSummary(membershipType, membershipId):
    output = makeRequest(BASE_URL+'/'+membershipType +
                         '/Account/'+membershipId+'/Summary/')
    # print(output.json()['ErrorStatus'])
    # saveToFile("accout summary.json", output.json())
    return output.json()


def accountHistory(membershipType, membershipId, characterId, count, page):
    output = makeRequest(BASE_URL+'/Stats/ActivityHistory/'+membershipType+'/'+membershipId +
                         '/'+characterId+'/?mode=none&count=' + str(count) + '&page=' + str(page))
    # print(output.json()['ErrorStatus'])
    # saveToFile("accout history.json", output.json())
    return output.json()


def getPGCR(gameId):
    # get from file
    # file = Path('PGCR/'+gameId+'.json')
    # if(file.is_file()):
    #     # print('file')
    #     return readJsonFile('PGCR/'+gameId+'.json')

    # get from api
    output = makeRequest(BASE_URL+'/Stats/PostGameCarnageReport/'+gameId+'/?definitions=true')
    # print(output.json()['ErrorStatus'])
    saveToFile("PGCR/"+str(gameId)+".json", output.json())
    return output.json()

# get player id
# get character ids
# get game ids
# get game reports
# formate and output


conn = sqlite3.connect("destiny.sqlite3")
c = conn.cursor()

# 1=xbox, ?=playstation
membershipId = ''
membershipType = ''
characterIds = []
gameIds = []

print('Searching for player ' + playerName)
data = searchForPlayer(playerName, '-1')

if(len(data['Response']) == 1):
    print('one player found')
    membershipId = str(data['Response'][0]['membershipId'])
    membershipType = str(data['Response'][0]['membershipType'])
else:
    print('More than one player found. Exiting.')

print('ID:' + str(membershipId) + " Type:" + str(membershipType))

print('Searching for account summary')
data = accountSummary(membershipType, membershipId)

for character in data['Response']['data']['characters']:
    characterIds.append(str(character['characterBase']['characterId']))
    print('Character ID:' + str(character['characterBase']['characterId']))

print('Searching for game ids')
for characterId in characterIds:
    print('Getting for character:' + characterId)

    count = 250
    page = 0
    gotCount = 250
    totalCount = 0

    while(gotCount == 250):
        print('Getting page:'+str(page))
        data = accountHistory(membershipType, membershipId, characterId, 250, page)
        # add each id to a list
        for game in data['Response']['data']['activities']:
            gameIds.append(str(game['activityDetails']['instanceId']))
        gotCount = len(data['Response']['data']['activities'])
        totalCount += gotCount
        if(gotCount == count):
            page += 1
        print('Got '+str(totalCount))
print('Total PGCR:'+str(len(gameIds)))

# **************** only new games have the data i want
# ****************  -gun with kills and percision kills
# save data
# game id
# date and time
# game mode / activity hash / referance id
#   player / character
#       gun
#           kills
#           percision kills
#       super
#           kills
#       other things
#           kills of enemies
#           percision kills of ememies
#           assists

print('getting pgcrs')
gotCount = 0
# for game in gameIds:
#     print('Getting game:'+ str(game) + ' ' + str(gotCount) + '/' + str(len(gameIds)))
#     data = getPGCR(game)
#     gotCount+=1

print('inserting')
GAME_SQL = 'INSERT INTO game(game_id, mode,date, time, referance_id, activity_hash) VALUES (?,?,?,?,?,?)'
PLAYER_SQL = 'INSERT INTO player(membership_id, membership_type, display_name, character_id, game_id) VALUES (?,?,?,?,?)'
GUN_SQL = 'INSERT INTO gun(gun_id, kills, percision_kills, player_id) VALUES (?,?,?,?)'

ex = []

# enter into database
for game in gameIds:
    print('Getting game:'+ str(game) + ' ' + str(gotCount) + '/' + str(len(gameIds)) + ' ex:'+str(len(ex)))
    data = getPGCR(game)
    gotCount+=1
    if('extended' in data['Response']['data']['entries'][0]):
        print('has extended ' + game)
        ex.append(game)
        continue
    else:
        print('*no extended ' + game)
        continue
    
    game_id = data['Response']['data']['activityDetails']['instanceId']
    mode = data['Response']['data']['activityDetails']['mode']
    date_time = data['Response']['data']['period']
    date = date_time.split('T')[0]
    time = date_time.split('T')[1]

    ref_id = data['Response']['data']['activityDetails']['referenceId']
    activity_hash = data['Response']['data']['activityDetails']['activityTypeHashOverride']

    # print(date + " " + time)

    try:
        c.execute(GAME_SQL, (game_id, mode, date, time, ref_id, activity_hash))
    except sqlite3.IntegrityError:
        print('game ERROR')
    
    for player in data['Response']['data']['entries']:
        table_id = ''
        membership_id = player['player']['destinyUserInfo']['membershipId']
        membership_type = player['player']['destinyUserInfo']['membershipType']
        display_name = '*ERROR'
        try:
            display_name = player['player']['destinyUserInfo']['displayName']
        except:
            print("No Display name")
        character_id = player['characterId']
        # game_id already have

        try:
            c.execute(PLAYER_SQL, (membership_id, membership_type, display_name, character_id, game_id))
        except sqlite3.IntegrityError:
            print('player ERROR')

        table_id = c.lastrowid

        try:
            for gun in player['extended']['weapons']:
                gun_id = gun['referenceId']
                kills = gun['values']['uniqueWeaponKills']['basic']['value']
                p_kills = gun['values']['uniqueWeaponPrecisionKills']['basic']['value']
                player_id = table_id

                try:
                    c.execute(GUN_SQL, (gun_id, kills, p_kills, player_id))
                except sqlite3.IntegrityError:
                    print('gun ERROR')
            print('extended ' + game_id + " " + display_name)
        except:
            # print('no extended ' + game_id + " " + display_name)
            a = ''
        # print(c.lastrowid)
        # conn.commit()

conn.commit()
conn.close()

print('ex '+str(len(ex)))

print('Done')
